package com.fullstack.shop.sys.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fullstack.common.exceptions.BusinessException;
import com.fullstack.common.service.impl.BaseServiceImpl;
import com.fullstack.shop.sys.dao.AttachDao;
import com.fullstack.shop.sys.entity.Attach;
import com.fullstack.shop.sys.service.AttachService;

/**
 * 
 * @author chay
 * @version 2017-04-17
 */
@Service
public class AttachServiceImpl extends BaseServiceImpl<AttachDao, Attach> implements AttachService<Attach>{

	@Override
	public boolean bindParentId(Integer id,Integer parentId,Integer type) throws BusinessException {
		Attach orginAttach = super.getInfoById(id);
		if(orginAttach==null){
			throw new BusinessException(9003004);
		}
		orginAttach.setParentId(parentId);
		orginAttach.setType(type);
		super.editById(orginAttach);
		return true;
	}

	@Override
	public Attach getLastAttach(Integer parentId,Integer type) throws BusinessException {
		Attach attach = new Attach();
		attach.setParentId(parentId);
		attach.setType(type);
		EntityWrapper<Attach> wrapper = this.entityInit(attach);
		wrapper.orderBy("id", false);
		return super.selectOne(wrapper);
	}

}