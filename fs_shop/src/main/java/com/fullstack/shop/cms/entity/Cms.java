package com.fullstack.shop.cms.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fullstack.common.exceptions.BusinessException;
import com.fullstack.common.persistence.DataEntity;

/**
 * 
 * @author Administrator
 *
 */
@TableName("shop_cms")
public class Cms extends DataEntity<Cms> {
	
	private static final long serialVersionUID = 1L;

	private Integer type;		// 类型
	private String title;		// 标题
	private String content;		// 内容
	private String url;			// 跳转地址
	private String path;		// 展示附件地址
	private String target;		// 展开方式
	
	
	@TableField("update_by")
	protected Integer updateBy;	// 修改者
	@TableField("update_date")
	protected Date updateDate;	// 修改日期
	


	
	@TableField(exist=false)
	private String typeDis;
	
	@Override
	public String toString(){
		StringBuffer str = new StringBuffer();
		str.append(" type："+this.type);
		return str.toString();
	}
	
	public Cms() throws BusinessException {
		super();
	}

	public Cms(Integer id){
		super(id);
	}

	
	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getTypeDis() {
		if(this.type == Cms.TYPE_BANNER) {
			return "首页导航";
		}
		return typeDis;
	}

	public void setTypeDis(String typeDis) {
		this.typeDis = typeDis;
	}



	public static int TYPE_BANNER = 1;		// 首页导航条
	
	
}