package com.fullstack.common.utils;

/**
 * 
 * @author chay
 * @version 
 */
public class ImgUtils {

	public static final String  IMG_KEY = "imgPath";
	

	/**
	 * 
	 */
	public static String commonPathUtils(String basePath,String entityPath,String fileName) {
		return basePath+entityPath+"/"+fileName;
	}
	
	public static String goodsImgPathUtils(Integer goodsId) {
		return "http://127.0.0.1:8888/goodsImg/file/download?goodsId="+goodsId;
	}
	
	public static String attachPathUtils(Integer parentId,Integer type) {
		return "http://127.0.0.1:8888/attach/download?parentId="+parentId+"&type="+type;
	}
	
	
	
	
	
	
	public static void main(String[] args) {
		System.out.println();
	}
}
